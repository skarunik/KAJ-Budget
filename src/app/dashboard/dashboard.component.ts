import { Component, OnInit }    from '@angular/core';
import { Router }               from '@angular/router';
import { UserService }          from '../service/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  constructor( private userService: UserService, private router: Router ) { }

  ngOnInit() {
    if ( !this.userService.isLogged() ){
      this.router.navigate(['/login']);
    }
  }

}
