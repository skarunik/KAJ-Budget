import { Component, OnInit } from '@angular/core';
import { Chart }             from 'chart.js';

import { BudgetService }     from '../service/budget.service';

import '../model/budget-item';
import '../model/budget';
import '../model/payment';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.css']
})
export class PiechartComponent implements OnInit {

  budgets: any[] = [];
  chart = [];
  datasets : any[] = [];
  labels = [];
  temp : any[] = []
  payments: Payment[];

  done : number = 0;
  curr : number = 0;

  palette = ['rgba(244, 67, 54, 0.6)', 'rgba(156, 39, 176, 0.6)', 'rgba(63, 81, 181, 0.6)', 'rgba(33, 150, 244, 0.6)', 'rgba(0, 188, 212, 0.6)', 'rgba(0, 150, 136, 0.6)', 'rgba(139, 195, 74, 0.6)', 'rgba(255, 235, 59, 0.6)', 'rgba(255, 152, 0, 0.6)', 'rgba(255, 87, 34, 0.6)', 'rgba(121, 85, 72, 0.6)', 'rgba(158, 158, 158, 0.6)', 'rgba(96, 125, 139, 0.6)']
  palette_border = ['rgba(210, 47, 47, 0.6)', 'rgba(123, 31, 162, 0.6)', 'rgba(48, 63, 159, 0.6)', 'rgba(25, 118, 210, 0.6)', 'rgba(0, 151, 167, 0.6)', 'rgba(0, 121, 107, 0.6)', 'rgba(104, 159, 56, 0.6)', 'rgba(251, 192, 45, 0.6)', 'rgba(245, 124, 0, 0.6)', 'rgba(230, 74, 25, 0.6)', 'rgba(93, 64, 55, 0.6)', 'rgba(97, 97, 97, 0.6)', 'rgba(69, 90, 99, 0.6)']

  constructor(private budgetService: BudgetService) { }

  ngOnInit() {
    let _a = new Dataset([], [], []);
    this.datasets.push(_a);
    this.loadPayments();
  }

  loadPayments(){
    this.budgetService.getPayments()
      .subscribe(res => {
        this.payments = res as Payment[];
      },
      (err) => console.error(err),
      () => {
        this.loadBudgets()
      });
  }

  loadBudgets(){
    this.budgetService.getBudgets()
      .subscribe(
        (res) => {
          this.budgets = res as Budget[];
        },
        (err) => console.error(err),
        () => {
          let _i = 0;
          for (let budget of this.budgets) {
            this.labels.push(budget.name);
            let _b = new BudgetSum(_i, budget.id, budget.name);
            this.temp.push(_b)
            _i++;
          }
          for (let budget of this.budgets) {
            this.done++;
            this.loadBudgetItems(budget.id);
          }
        }
    );
  }

  loadBudgetItems(budgetId){
    var budgetItems = [];
    this.budgetService.getBudgetItems( budgetId )
      .subscribe(
        res => {
          budgetItems = res as BudgetItem[];
        },
        (err) => console.error(err),
        () => {
          this.curr++;
          for (let budgetSum of this.temp){
            if (budgetSum.budgetId == budgetId){
              for (let item of budgetItems){
                budgetSum.budgetItems.push(item);
              }
              break;
            }
          }
          this.loadSum(budgetId);
        }
    );
  }

  loadSum(budgetId){
    for (let budgetSum of this.temp){
      if (budgetSum.budgetId == budgetId){

        for (let payment of this.payments){
          for (let budgetItem of budgetSum.budgetItems){
            if (payment.budget_item_id == budgetItem.id){
              budgetSum.money += payment.amount;
            }
          }
        }

        break;
      }
    }
    this.buildChart();
  }

  buildChart(){
    for (let _i = 0; _i < this.labels.length; _i++){
      for (let data of this.temp){
        if (data.name == this.labels[_i]){
          this.datasets[0].data[data.index] = data.money;
          this.datasets[0].backgroundColor.push(this.palette[_i]);
          this.datasets[0].borderColor.push(this.palette_border[_i]);
        }
      }
    }
    this.chart = new Chart('piechart', {
        type: 'pie',
    data: {
      datasets: this.datasets,
      labels: this.labels
    },
    options: {
      responsive: true
    }
    })
  }

}

class Dataset {

  data: any[];
  backgroundColor: any[];
  borderColor: any[];

  constructor(data: any, bgc: any, border: any){

    this.data = data;
    this.backgroundColor = bgc;
    this.borderColor = border;
  }
}

class BudgetSum {
  index: number;
  budgetId: number;
  budgetItems: any[];
  money: number;
  name: string;

  constructor(index: any, id: number, name: string){
    this.index = index;
    this.budgetId = id;
    this.money = 0;
    this.budgetItems = [];
    this.name = name;
  }
}
