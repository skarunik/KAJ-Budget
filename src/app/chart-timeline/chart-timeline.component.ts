import { Component, OnInit }  from '@angular/core';
import { Chart }              from 'chart.js';

import { BudgetService }      from '../service/budget.service';

import '../model/budget-item';
import '../model/budget';
import '../model/payment';

@Component({
  selector: 'app-chart-timeline',
  templateUrl: './chart-timeline.component.html',
  styleUrls: ['./chart-timeline.component.css']
})
export class ChartTimelineComponent implements OnInit {

  budgets: any[];
  selectedBudget: any;
  budgetItems: BudgetItem[];

  payments: Payment[];
  chart = [];

  public x_dates : Date[] = [];
  public x_str : string[] = [];
  public palette = ['rgba(244, 67, 54, 0.85)', 'rgba(156, 39, 176, 0.85)', 'rgba(63, 81, 181, 0.85)', 'rgba(33, 150, 244, 0.85)', 'rgba(0, 188, 212, 0.85)', 'rgba(0, 150, 136, 0.85)', 'rgba(139, 195, 74, 0.85)', 'rgba(255, 235, 59, 0.85)', 'rgba(255, 152, 0, 0.85)', 'rgba(255, 87, 34, 0.85)', 'rgba(121, 85, 72, 0.85)', 'rgba(158, 158, 158, 0.85)', 'rgba(96, 125, 139, 0.85)']
  public palette_border = ['rgba(210, 47, 47, 0.85)', 'rgba(123, 31, 162, 0.85)', 'rgba(48, 63, 159, 0.85)', 'rgba(25, 118, 210, 0.85)', 'rgba(0, 151, 167, 0.85)', 'rgba(0, 121, 107, 0.85)', 'rgba(104, 159, 56, 0.85)', 'rgba(251, 192, 45, 0.85)', 'rgba(245, 124, 0, 0.85)', 'rgba(230, 74, 25, 0.85)', 'rgba(93, 64, 55, 0.85)', 'rgba(97, 97, 97, 0.85)', 'rgba(69, 90, 99, 0.85)']
  public sorted_payments: ItemsPayments[] = [];
  public datasets: any[] = [];

  constructor(private budgetService: BudgetService) { }

  ngOnInit() {
    this.budgetService.getBudgets()
      .subscribe(
        (res) => {
          this.budgets = res as Budget[];
          this.buildChart();
        },
        (err) => console.error(err),
        () => {}
    );
    this.getLastWeek();
  }


  loadBudgetItems(){
    this.budgetService.getBudgetItems( this.selectedBudget )
      .subscribe(
        res => {
          this.budgetItems = res as BudgetItem[];
        },
        (err) => console.error(err),
        () => {
          this.loadPayments();
        }
    );
  }

  loadPayments(){
    this.budgetService.getPayments()
      .subscribe(res => {
        this.payments = res as Payment[];
      },
      (err) => console.error(err),
      () => {
        for (let item of this.budgetItems){
          let list = new ItemsPayments(item.id, [], item.name);
          this.sorted_payments.push(list);
        }
        for (var payment of this.payments){
          for (var list of this.sorted_payments){
            if (payment.budget_item_id == list.item){
              list.payments.push(payment);
              break;
            }
          }
        }
        this.loadDatasets();
      });
  }

  loadDatasets(){
    var index = 0;
    for (let set of this.sorted_payments){
      let data = [];
      let color = this.palette[index];
      let color2 = this.palette_border[index];
      let label = set.name;
      for (let _i = 0; _i < this.x_dates.length; _i++){
        let sum = 0;
        for (let payment of set.payments){
          let a = new Date(payment.date);
          let date = new Date(a.getFullYear(), a.getMonth(), a.getDate());
          if (date.getTime() == this.x_dates[_i].getTime()){
            sum += payment.amount;
          }
        }
        data.push(sum);
      }
      var _dataset = new dataset(label, color, color2, data);
      this.datasets.push(_dataset);
      index++;
    }
    this.buildChart();
  }


  buildChart() {
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: this.x_str,
        datasets: this.datasets
      },
      options: {
        responsive: true,
        title: {
          display: false,
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Timestamp'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Spending $'
            }
          }]
        }
      }
    })
  }

  getLastWeek(){
    let today = new Date();
    for (var _i = 6; _i > -1; _i--) {
      let newDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - _i);
      let str = (+newDay.getDate()+1) + "." + (+newDay.getMonth()+1);
      this.x_str.push(str);
      this.x_dates.push(newDay);
    }
  }

}

class dataset {
  label: any;
  backgroundColor: any;
  borderColor: any;
  data: any;
  fill: any;
  constructor (label: any, backgroundColor: any, borderColor: any, data: any){
    this.label = label;
    this.backgroundColor = backgroundColor;
    this.borderColor = borderColor;
    this.data = data;
    this.fill = true;
  }
}

class ItemsPayments {

  item: number;
  name: string;
  payments: any[];

  constructor (item: any, payments: any, name: any){
    this.item =item;
    this.payments = payments;
    this.name = name;
  }
}
