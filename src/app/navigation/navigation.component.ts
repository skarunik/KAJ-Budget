import { Component, OnInit }    from '@angular/core';
import { Router }               from '@angular/router';
import { UserService }          from '../service/user.service';
import { BudgetService }        from '../service/budget.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  projectName: string;
  projectDescription: string;
  output: any;

  constructor( private userService: UserService, private budgetService: BudgetService, private router: Router ) { }

  ngOnInit() {
  }

  createBudget(){
    if ( this.projectName && this.projectDescription ){
      this.budgetService.newBudget(this.projectName, this.projectDescription)
        .subscribe (
          (data) => {
            this.output = "Budget was created";
            this.projectDescription = "";
            this.projectName = "";
          },
          (err) => {this.output = err.error.error.message},
          () => {}
        );
    } else {
      this.output = 'Missing parameters!';
    }

  }

  logout(){
    this.userService.logout();
    this.router.navigate(['/login']);
  }

}
