class BudgetItem {

  constructor(id: number, name: string, budget_id: number, created_at: string, updated_at: string, user_id: number){
    this.id = id;
    this.name = name;
    this.budget_id = budget_id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.user_id = user_id;
    this.payments = [];
  }

  id: number;
  name: string;
  budget_id: number;
  created_at: string;
  updated_at: string;
  user_id: number;
  payments: any[];
}
