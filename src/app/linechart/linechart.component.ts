import { Component, OnInit } from '@angular/core';
import { Chart }             from 'chart.js';

import { BudgetService }     from '../service/budget.service';

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.css']
})
export class LinechartComponent implements OnInit {

    chart = [];

    constructor(private budgetService: BudgetService) { }

    ngOnInit() {
      this.budgetService.getBudgets()
        .subscribe(res => {

        let data = [7, 2, 5, 4]
        // let colors = ["#ffb3ba", "#ffdfba", "#ffffba", "#baffc9", "#bae1ff"]
        let colors = ["rgba(251, 173, 180, 0.7)", "rgba(255, 223, 186, 0.7)", "rgba(237, 237, 175, 0.7)", "rgba(161, 218, 174, 0.7)", "rgba(168, 202, 228, 0.7)"]
        let labels = ["2018-01", "2018-02", "2018-03", "2018-04"]

        this.chart = new Chart('linechart', {
            type: 'bar',
			data: {
				labels: labels,
				datasets: [{
					label: 'CHRT - Chart.js Corporation',
					data: data,
					type: 'line',
					pointRadius: 0,
					fill: false,
					lineTension: 0,
					borderWidth: 2
				}]
			},
			options: {
				scales: {
					xAxes: [{
						type: 'time',
						distribution: 'series',
						ticks: {
							source: 'labels'
						}
					}],
					yAxes: [{
						scaleLabel: {
							display: true,
							labelString: 'Closing price ($)'
						}
					}]
				}
			}
        }) // polar ends

      }); // Subscribe end
    }

}
