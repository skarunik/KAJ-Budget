# Budget v1.0 - simple budget planner

**[Live demo](http://13.57.38.60)** runs on AWS

User with prepared data:
- email: user@example.com
- password: 1qaz2wsx

## Backend

**[Restfull API repository](https://gitlab.fel.cvut.cz/skarunik/KAJ-Budget-API)** - here you can find source code of API used for this application written on Ruby.

## Used technologies

- Angular 5
    - single page application
    - [reference](https://angular.io/)
- TypeScript
- Bootstrap 3
    - used mainly because of grid system
    - [reference](https://getbootstrap.com/)
- FontAwesome
    - icons
    - [reference](https://fontawesome.com/)
- Google Maps
    - map on About us page
- Charts.js
    - used for all charts
    - [reference](https://www.chartjs.org/)
- JWTtokens
    - used for authorization authentication
    - [reference](https://jwt.io/)

## Description

This application help you analyze you spendings. You can create Budgets, Budget Items and Payment. Here is an example of structure:

- Nick's budget
    - Food spendings
        - 2$ snickers 8-06-2018
        - 12$ pizza 11-06-2018
    - Clothes spendings
        - 100$ new shoes 10-06-2018

After adding data you can see analytic charts.

On **Analisis page** you can find 3 chart. First one shows all spendings of each budget and compare them in piechart. Two others are disabled (shows fake data). 

The other chart is **Timelime diagram**. You can see payment of selected Budget item on timeline.

## Bugs

Here is a list of known bugs:

- Logout - sometimes action doesn't redirec you to login page
- Timeline Chart - when you change your selection, old data combines with new data

This errors can be fixed by reloading page.

## Conclusion

Implementing this application was usefull experiance for me. I've started to learn Angular with this app and I was very excited so now i have another Angular projects. I decided to implement pretty big application and as a result my application turned out a little bit laggy and contains a few bugs, but it is fully functional.